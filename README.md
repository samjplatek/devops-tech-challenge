# Health Union DevOps Challenge

## The Challenge

Create a YUM repository and host the three RPM packages found in this Git repository. The standard RPM package management tool in Fedora, Red Hat Enterprise Linux, and CentOS is the yum package manager.
These yum packages can be stored in a centralized location called a yum repository. Creating your own yum repository is very simple, and very straightforward. There are TONS of
documentation online to help you achieve this task.

## Specifications

- Use Centos 7.
- Host the YUM repository over the internet. Use either a personal server or a cloud provider such as Google Cloud Platform or AWS. [GCP](https://cloud.google.com/free) and [AWS](https://aws.amazon.com/free/) offer free-tier
- Download the three RPM packages found in this Git repo and upload to your YUM repository.
- The repository needs to be available via HTTP in order for other servers to pull from it. You'll also need another machine to act as the client.

## Outcome

- Explain why you would want to host your own YUM repository (verbally).
- Create a bash script to automate setting up the YUM repository. You do not need to automate the part where you seed the repository with sample packages.
- Show that you can install the packages on your client machine.
- I will have a server of my own where I will make an attempt to download packages from your YUM repo. You will supply me the config I need to install on my machine.
- We'll play a version of “Wheel of misfortune” where I will simulate a couple of issues and you'll walk me through the troubleshooting steps. 

## Rules

- I expect you to Google for the answer. I'm sure you'll be able to accomplish this task by copying the steps from the thousands of articles you find online. The key is whether you can troubleshoot and explain the steps you took in order to set up the YUM repository.
- Your YUM repository has to be accessible via the internet. If you need help with this, I will be more than happy to help you.
- Fork this repository and submit a PR or a separate branch under your own repository. DO NOT submit a PR against the origin. There are two files you need to submit.
    - The script to set up the YUM repository. 
    - The YUM config that resides on the client in order to connect to the YUM repository.

## Advice

- Disable GPGCHECK on the client to make your life easier. Bonus points if you understand what that option is used for and if you manage to get it to work correctly.
- Stick to APache for your webserver. Some articles may reference Nginx, Apache should suffice and is a bit easier to set up.
- Disable the [firewall](https://linuxize.com/post/how-to-stop-and-disable-firewalld-on-centos-7/) and [selinux](https://linuxize.com/post/how-to-disable-selinux-on-centos-7/) to make your life easier.